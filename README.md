# MASK R-CNN Project

## Introduction
In this project I have implemented MASK R-CNN deep learning model which is the extension of Faster R-CNN that is being used for the object detection. This model has been introduced in 2017 in the paper “Mask R-CNN” and is still widely used for many projects. The key improvement of Mask R-CNN over Faster R-CNN is an addition of a prediction of an object mask at the same time with the bounding box recognition. (viso.ai, 2021)

## Architecture 
MASK R-CNN has two major parts for the classification of the objects. It derives the idea of object detection from the Faster R-CNN which includes instance segmentation “Driven by the effectiveness of R-CNN, many approaches to instance segmentation are based on segment proposals.”. He, K. et al. (2017) To meet the requirements for object detection it is it necessary to classify each object and localize the object. In this scenario the model uses Convolutional neural Network (CNN). The researchers used ResNet architecture to extract features from the input image and then passes them to Region Proposal Network (RPN) to generate bounding boxes by sliding a small network over the feature map. The loss function in this network is required to decrease the number of wrong predictions for incorrect object proposals. This network allows to make predictions for each anchor box location. (Khandelwal, 2019)

The next step is segmenting each instance by using instance segmentation. Instance segmentation is responsible for identification and separation of the objects from the image by identification of the corresponding pixels. The module region of Interest (ROI) Align is being used to extract features and prepare it for classification and for segmentation. During classification, the loss function uses cross- entropy loss penalizes the model for making incorrect object classific

In MASK R-CNN as in previous proposed model it is necessary to repeat the process of bounding box and classification for detection objects. In this stage it is necessary to do regression which will repeat the process by predicting the coordinates of the objects. The loss function is being used to penalize the model for incorrect prediction of the object bounding box. The last stage is the mask prediction, which uses binary mask for every proposed object to indicate pixel-wise location of the object

## Requirements


## Getting started
Download ipynb 

## References
Khandelwal, R. (2019). Computer Vision: Instance Segmentation with Mask R-CNN. [online] Medium. Available at: https://towardsdatascience.com/computer-vision-instance-segmentation-with- mask-r-cnn-7983502fcad1

viso.ai. (2021). Mask R-CNN: A Beginner’s Guide. [online] Available at: https://viso.ai/deep-learning/mask-r-cnn/.

He, K. et al. (2017) ‘Mask R-CNN’. arXiv. doi: 10.48550/ARXIV.1703.0687
